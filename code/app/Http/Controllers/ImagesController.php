<?php

namespace App\Http\Controllers;

use App\Image;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use Exception;
use App\Http\Requests\ImageRequest;
use Illuminate\Contracts\Filesystem\Factory;
use Illuminate\Filesystem\Filesystem;

class ImagesController extends Controller
{

    protected $storage;

    protected $file;

    public function __construct(Factory $storage, Filesystem $file)
    {
        $this->storage = $storage;
        $this->file = $file;
    }

    public function uploadImage(ImageRequest $request)
    {
        $uploadedFile = $request->file('image');
        $extension = $uploadedFile->getClientOriginalExtension();

        $fileName = uniqid() . ".$extension";

        try {
            $s3 = $this->storage->disk('s3');

            $s3->put($fileName, $this->file->get($uploadedFile->getRealPath()));

            Image::create([

                'path' => 'storage/'.$fileName
            ]);

            return response()->json(['success' => 'image uploaded'], 200);

        } catch (Exception $exception) {

            return response()->json(['exception' => 'Not able to store image, details:' . $exception->getMessage()],
                400);
        }
    }


    public function listImages()
    {
        try {
            $images = Image::all();

            return response()->json(['images' => $images], 200);

        } catch (Exception $exception) {

            return response()->json(['exception' => 'error:' . $exception->getMessage()], 400);
        }
    }

    public function deleteImage($id)
    {
        try {

            $image = Image::where('id', $id)->first();

            File::delete($image->path);

            $image->delete();

            return response()->json(['success' => 'image deleted'], 200);

        } catch (Exception $exception) {

            return response()->json(['exception' => 'error:' . $exception->getMessage()], 400);
        }
    }
}
