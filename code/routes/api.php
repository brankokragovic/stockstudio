<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/uploadImage', 'ImagesController@uploadImage');
Route::get('/listImages','ImagesController@listImages');
Route::delete('/deleteImage/{id}','ImagesController@deleteImage');

Route::post('/createAlbum','AlbumsController@createAlbum');
Route::get('/listAlbums','AlbumsController@listAlbums');
Route::post('/editAlbum','AlbumsController@editAlbum');
Route::delete('/deleteAlbum/{id}','AlbumsController@deleteAlbum');

Route::post('/createPoster','PostersController@createPoster');
Route::post('/editPosterTitle','PostersController@editPosterTitle');
Route::post('/editPosterDescription','PostersController@editPosterDesc');
Route::delete('/deletePoster/{id}','PostersController@deletePoster');
