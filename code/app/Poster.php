<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Poster extends Model
{
    protected $fillable = [
        'name',
        'path' ,
        'album_id',
        'image_id',
        'title_path',
        'desc_path'
    ];
}
