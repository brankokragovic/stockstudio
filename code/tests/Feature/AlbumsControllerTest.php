<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AlbumsControllerTest extends TestCase
{

    public function testListAlbums()
    {
        $response = $this->get('/api/listAlbums');

        $response->assertStatus(200);
    }


    public function testCreateAlbum(){

        $response = $this->post('/api/createAlbum',[
            'name' => 'test'
        ]);

        $response->assertStatus(200);

    }

    public function testDeleteAlbum(){

        $response = $this->delete('/api/deleteAlbum/1');

        $response->assertStatus(200);

    }
}
