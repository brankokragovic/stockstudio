<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class ImagesControllerTest extends TestCase
{


    public function testUploadImage(){

        Storage::fake('s3');

       $response = $this->postJson('api/uploadImage', [
            'image' => UploadedFile::fake()->image('5f936233258b2.jpg')
        ]);


        $response->assertStatus(200);

    }
    public function testListImages()
    {
        $response = $this->get('/api/listImages');

        $response->assertStatus(200);
    }

    public function testDeleteImage(){

        $response = $this->delete('/api/deleteImage/1');

        $response->assertStatus(200);

    }
}
