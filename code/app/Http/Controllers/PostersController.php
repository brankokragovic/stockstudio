<?php

namespace App\Http\Controllers;

use App\Album;
use App\Image;
use Illuminate\Http\Request;
use App\Helpers\PosterHelper;
use App\Poster;
use Illuminate\Contracts\Filesystem\Factory;
use Illuminate\Support\Facades\File;

class PostersController extends Controller
{

    protected $helper;

    protected $storage;

    public function __construct(PosterHelper $posterHelper,Factory $storage)
    {
        $this->helper = $posterHelper;
        $this->storage = $storage;

    }


    public function createPoster(Request $request)
    {

        try {
            $image = Image::where('id', $request->image_id)->first();

            $album = Album::where('id', $request->album_id)->first();

            $type = $this->helper->type($image->path);


            Poster::create([
                'name' => $request->name,
                'image_id' => $request->image_id,
                'path' => 'storage/albums/' . $album->name . '/' . $request->name . '.' . $type,
                'title_path'=>'storage/albums/' . $album->name . '/title/' . $request->name . '.' . $type,
                'desc_path'=>'storage/albums/' . $album->name . '/desc/' . $request->name . '.' . $type,
                'album_id' => $request->album_id

            ]);

             $this->helper->makeOgPoster($image->path, $request->title, $request->desc, $album->name, $request->name);

             return response()->json(['success'=>'poster created'],200);

        }catch (\Exception $exception){

            return response()->json(['exception' => 'error:' . $exception->getMessage()],400);
        }
    }



    public function editPosterTitle(Request $request)
    {

        try{

        $poster = Poster::where('id', $request->poster_id)->first();

        $album = Album::where('id', $poster->album_id)->first();

        $image = Image::where('id', $poster->image_id)->first();

        $this->helper->makeTitleImage($image->path, $request->title, $album->name, $poster->name);

        $this->helper->makeOgPoster($image->path, $request->title, $request->desc, $album->name, $poster->name);

            return response()->json(['success'=>'title edited'],200);

        }catch (\Exception $exception){

            return response()->json(['exception' => 'error:' . $exception->getMessage()],400);
        }
    }



    public function editPosterDesc(Request $request)
    {

        try {
            $poster = Poster::where('id', $request->poster_id)->first();

            $album = Album::where('id', $poster->album_id)->first();

            $image = Image::where('id', $poster->image_id)->first();

            $this->helper->makeDescImage($image->path, $request->desc, $album->name, $poster->name);

            $this->helper->makeOgPoster($image->path, $request->title, $request->desc, $album->name, $poster->name);

            return response()->json(['success'=>'description edited'],200);

        }catch (\Exception $exception){

            return response()->json(['exception' => 'error:' . $exception->getMessage()],400);
        }

    }

    public function deletePoster($id){

        $poster = Poster::where('id',$id)->first();

        $s3 = $this->storage->disk('s3');
        $paths = [$poster->path,$poster->title_path,$poster->desc_path];
        $s3->delete($poster->path);
        File::delete($paths);
        $poster->delete();
    }
}
