<?php

namespace App\Http\Controllers;

use App\Album;
use App\Http\Requests\AlbumRequest;
use App\Poster;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Contracts\Filesystem\Factory;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class AlbumsController extends Controller
{

    protected $storage;

    public function __construct(Factory $storage)
    {
        $this->storage = $storage;
    }

    public function createAlbum(AlbumRequest $request)
    {

        try {

            $name = $request->name;

            $path = 'albums/' . $name;
            $path1 = 'albums/' . $name . '/title/';
            $path2 = 'albums/' . $name . '/desc/';
            $s3 = $this->storage->disk('s3');

            $s3->makeDirectory($path);
            $s3->makeDirectory($path1);
            $s3->makeDirectory($path2);

            Album::create([
                'name' => $name
            ]);

            return response()->json(['success' => 'album created'], 200);

        } catch (Exception $exception) {

            return response()->json(['exception' => 'error:' . $exception->getMessage()], 401);

        }
    }

    public function editAlbum(Request $request)
    {

        try {
            $album = Album::where('id', $request->album_id)->first();
            $poster = Poster::where('album_id', $request->album_id)->first();

            Storage::rename('public/albums/' . $album->name, 'public/albums/' . $request->name);

            $album->name = $request->name;
            $album->update();

            if ($poster) {
                $poster_path = basename($poster->path);

                $path = 'storage/albums/' . $request->name . '/' . $poster_path;
                $path1 = 'storage/albums/' . $request->name . '/title' . $poster_path;
                $path2 = 'storage/albums/' . $request->name . '/desc/' . $poster_path;

                $poster->path = $path;
                $poster->title_path = $path1;
                $poster->desc_path = $path2;
                $poster->update();
            }
            return response()->json(['success' => 'album updated'], 200);

        } catch (Exception $exception) {

            return response()->json(['exception' => 'error:' . $exception->getMessage()], 400);

        }

    }

    public function listAlbums()
    {

        try {
            $albums = Album::all();

            return $albums;

        } catch (Exception $exception) {

            return response()->json(['exception' => 'error:' . $exception->getMessage()], 400);

        }

    }

    public function deleteAlbum($id)
    {

        try {
            $album = Album::whereId($id)->first();

            File::deleteDirectory('storage/albums/' . $album->name);

            Poster::where('album_id', $id)->delete();

            $album->delete();

            return response()->json(['success' => 'album deleted'], 200);

        } catch (Exception $exception) {

            return response()->json(['exception' => 'error:' . $exception->getMessage()], 400);

        }

    }
}
