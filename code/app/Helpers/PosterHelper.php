<?php

namespace App\Helpers;

class PosterHelper
{


    public function makeOgPoster($image, $title, $desc, $album, $posterName)
    {

            $type = $this->type($image);

            list($new_width, $new_height) = getimagesize($image);

            $new_image_p = imagecreatetruecolor($new_width, $new_height);

            if ($type == "png") {
                imagecolortransparent($new_image_p, imagecolorallocate($new_image_p, 0, 0, 0));
            }
            if ($type == 'jpg') {
                $new_temp_image = imagecreatefromjpeg($image);
            }
            if ($type == 'png') {
                $new_temp_image = imagecreatefrompng($image);
            }
            imagecopyresampled($new_image_p, $new_temp_image, 0, 0, 0, 0, $new_width, $new_height, $new_width,
                $new_height);

            $adj_width = $new_width + 40;
            $adj_height = $new_height + 120;

            $bkgrd = imagecreatetruecolor($adj_width, $adj_height);
            imagefilledrectangle($bkgrd, 0, 0, $adj_width, $adj_height, 0x000000);
            $sx = imagesx($bkgrd) - imagesx($bkgrd) + 20;
            $sy = imagesy($bkgrd) - imagesy($bkgrd) + 20;

            imagecopymerge($bkgrd, $new_image_p, $sx, $sy, 0, 0, imagesx($bkgrd), imagesy($bkgrd), 100);

            $storage = 'storage/albums/' . $album . '/' . $posterName . '.' . $type;
            if ($type == "jpg") {

                imagejpeg($bkgrd, $storage, 100);
            }

            if ($type == "png") {

                imagepng($bkgrd, $storage, 100);
            }
            imagedestroy($new_image_p);
            imagedestroy($bkgrd);

            if ($title) {
                $this->makeTitleImage($storage, $title, $album, $posterName);

            }
            if ($desc) {
                $this->makeDescImage($storage, $desc, $album, $posterName);
            }

            $this->mergeImages($image, $new_height, $new_width, $new_temp_image, $album, $posterName);


    }


    public function calculateTextBox($text, $fontFile, $fontSize, $fontAngle)
    {

        $rect = imagettfbbox($fontSize, $fontAngle, $fontFile, $text);
        $minX = min(array($rect[0], $rect[2], $rect[4], $rect[6]));
        $maxX = max(array($rect[0], $rect[2], $rect[4], $rect[6]));
        $minY = min(array($rect[1], $rect[3], $rect[5], $rect[7]));
        $maxY = max(array($rect[1], $rect[3], $rect[5], $rect[7]));

        return array(
            "left" => abs($minX) - 1,
            "top" => abs($minY) - 1,
            "width" => $maxX - $minX,
            "height" => $maxY - $minY,
            "box" => $rect
        );

    }


    public function makeTitleImage($image, $text_string = null, $album, $posterName)
    {

        $type = $this->type($image);

        $font_ttf = "storage/fonts/LABTDU__.ttf";
        $font_size = 22;
        $text_angle = 0;
        $text_padding = 10;

        $the_box = $this->calculateTextBox($text_string, $font_ttf, $font_size, $text_angle);

        $imgWidth = $the_box["width"] + $text_padding;
        $imgHeight = $the_box["height"] + $text_padding;

        $image = imagecreatetruecolor($imgWidth, $imgHeight);
        imagefill($image, 0, 0, imagecolorallocate($image, 0, 0, 0));

        $color = imagecolorallocate($image, 255, 255, 255);

        imagettftext($image,
            $font_size,
            $text_angle,
            $the_box["left"] + ($imgWidth / 2) - ($the_box["width"] / 2),
            $the_box["top"] + ($imgHeight / 2) - ($the_box["height"] / 2),
            $color,
            $font_ttf,
            $text_string);

        if ($type == "jpg") {

            imagejpeg($image, 'storage/albums/' . $album . '/title/' . $posterName . '.' . $type, 100);
        }

        if ($type == "png") {
            imagepng($image, 'storage/albums/' . $album . '/title/' . $posterName . '.' . $type, 100);
        }

        imagedestroy($image);


    }

    public function makeDescImage($image, $text_string = null, $album, $posterName)
    {

        $type = $this->type($image);

        $font_ttf = "storage/fonts/high_sans_serif_7.ttf";
        $font_size = 16;
        $text_angle = 0;
        $text_padding = 10; // Img padding - around text

        $the_box = $this->calculateTextBox($text_string, $font_ttf, $font_size, $text_angle);

        $imgWidth = $the_box["width"] + $text_padding;
        $imgHeight = $the_box["height"] + $text_padding;

        $image = imagecreatetruecolor($imgWidth, $imgHeight);
        imagefill($image, 0, 0, imagecolorallocate($image, 0, 0, 0));

        $color = imagecolorallocate($image, 255, 255, 255);

        imagettftext($image,
            $font_size,
            $text_angle,
            $the_box["left"] + ($imgWidth / 2) - ($the_box["width"] / 2),
            $the_box["top"] + ($imgHeight / 2) - ($the_box["height"] / 2),
            $color,
            $font_ttf,
            $text_string);

        if ($type == "jpg") {

            imagejpeg($image, 'storage/albums/' . $album . '/desc/' . $posterName . '.' . $type, 100);
        }

        if ($type == "png") {
            imagepng($image, 'storage/albums/' . $album . '/title/' . $posterName . '.' . $type, 100);
        }

        imagedestroy($image);


    }

    public function mergeImages($image, $new_height, $new_width, $new_temp_image, $album, $posterName)
    {

        $type = $this->type($image);

        $filename_vs = 'storage/albums/' . $album . '/' . $posterName . '.' . $type;
        $filename_x = 'storage/albums/' . $album . '/title/' . $posterName . '.' . $type;
        $filename_y = 'storage/albums/' . $album . '/desc/' . $posterName . '.' . $type;
        list($width_x, $height_x) = getimagesize($filename_x);
        list($width_y, $height_y) = getimagesize($filename_y);
        list($width_vs, $height_vs) = getimagesize($filename_vs);


        if ($width_x > $new_width) {
            $line1_img_src = 'storage/albums/' . $album . '/title/' . $posterName . '.' . $type;
            $line1_img_path = 'storage/albums/' . $album . '/title/' . $posterName . '.' . $type;
            list($src_width, $src_height) = getimagesize($line1_img_src);

            $min = $new_width; // set our max width to that of the original image
            $ratio = $src_height / $src_width;
            $line1_width = $min;
            $Line1_height = round($min * $ratio);

            $blank_image_line1 = imagecreatetruecolor($line1_width, $Line1_height);

            imagecolortransparent($blank_image_line1, imagecolorallocate($blank_image_line1, 0, 0, 0));


            if ($type == 'jpg') {
                $image = imagecreatefromjpeg($line1_img_src);

            }
            if ($type == 'png') {
                $image = imagecreatefrompng($line1_img_src);


            }

            imagecopyresampled($blank_image_line1, $image, 0, 0, 0, 0, $line1_width, $Line1_height, $src_width,
                $src_height);


            if ($type == 'jpg') {
                imagejpeg($blank_image_line1, $line1_img_path, 100);

            }
            if ($type == 'png') {
                imagepng($blank_image_line1, $line1_img_path, 100);


            }

            imagedestroy($blank_image_line1);
            imagedestroy($image);

            $filename_x = 'storage/albums/' . $album . '/title/' . $posterName . '.' . $type;
            list($width_x, $height_x) = getimagesize($filename_x);
        }

        if ($width_y > $new_width) {

            $line2_img_src = 'storage/albums/' . $album . '/desc/' . $posterName . '.' . $type;
            $line2_img_path = 'storage/albums/' . $album . '/desc/' . $posterName . '.' . $type;
            list($src_width, $src_height) = getimagesize($line2_img_src);

            $min = $new_width;
            $ratio = $src_height / $src_width;
            $line2_width = $min;
            $line2_height = round($min * $ratio);

            $blank_image_line2 = imagecreatetruecolor($line2_width, $line2_height);

            imagecolortransparent($blank_image_line2, imagecolorallocate($blank_image_line2, 0, 0, 0));

            if ($type == 'jpg') {
                $image = imagecreatefromjpeg($line2_img_src);

            }
            if ($type == 'png') {
                $image = imagecreatefrompng($line2_img_src);


            }

            imagecopyresampled($blank_image_line2, $image, 0, 0, 0, 0, $line2_width, $line2_height, $src_width,
                $src_height);


            if ($type == 'jpg') {
                imagejpeg($blank_image_line2, $line2_img_path . '.' . $type, 100);

            }
            if ($type == 'png') {
                imagepng($blank_image_line2, $line2_img_path . '.' . $type, 100);

            }

            imagedestroy($blank_image_line2);
            imagedestroy($image);

            $filename_y = 'storage/albums/' . $album . '/desc/' . $posterName . '.' . $type;
            list($width_y, $height_y) = getimagesize($filename_y);
        }

        $image = imagecreatetruecolor($width_vs, $height_vs);

        if ($type == "png") {
            imagecolortransparent($image, imagecolorallocate($image, 0, 0, 0));
        }
        if ($type == 'jpg') {
            $image_vs = imagecreatefromjpeg($filename_vs);
            $image_x = imagecreatefromjpeg($filename_x);
            $image_y = imagecreatefromjpeg($filename_y);
        }
        if ($type == 'png') {
            $image_vs = imagecreatefrompng($filename_vs);
            $image_x = imagecreatefrompng($filename_x);
            $image_y = imagecreatefrompng($filename_y);
        }


        $vs_x = imagesx($image);
        $vs_y = imagesy($image);
        $x_x = $width_x;
        $x_x = ($vs_x / 2) - ($x_x / 2);
        $x_y = $new_height + 30;
        $y_x = $width_y;
        $y_x = ($vs_x / 2) - ($y_x / 2);
        $y_y = $new_height + 70;

        imagecopy($image, $image_vs, 0, 0, 0, 0, $width_vs, $height_vs);
        imagecopy($image, $image_x, $x_x, $x_y, 0, 0, $width_vs, $height_vs);
        imagecopy($image, $image_y, $y_x, $y_y, 0, 0, $width_vs, $height_vs);


        $adj_width = $new_width + 40;
        $adj_height = $new_height + 120;

        $bkgrd = imagecreatetruecolor($adj_width, $adj_height);
        imagefilledrectangle($bkgrd, 0, 0, $adj_width, $adj_height, 0x000000);
        $sx = imagesx($bkgrd) - imagesx($bkgrd) + 20;
        $sy = imagesy($bkgrd) - imagesy($bkgrd) + 20;

        imagecopymerge($bkgrd, $new_temp_image, $sx, $sy, 0, 0, imagesx($bkgrd), imagesy($bkgrd), 100);

        if ($type == "jpg") {
            imagejpeg($image, $filename_vs, 100);
        }
        if ($type == "png") {
            imagepng($image, $filename_vs, 100);
        }

        imagedestroy($image);
        imagedestroy($image_x);
        imagedestroy($image_y);
        imagedestroy($image_vs);

    }

    public function type($image)
    {

        $type = strtolower(substr(strrchr($image, "."), 1));

        return $type;

    }

}


